// Aop = {
//     around: function(fnName, advice, fnObj) {
//         // 처음 버전이라 하는 일이 없음
//         fnObj[fnName] = advice;
//     },
// };

class Aop {
    around(fnName, advice, fnObj) {
        const originalFn = fnObj[fnName];
        fnObj[fnName] = () => {
            const targetContext = {}; // Wrong code...
            advice.call(targetContext, { fn: originalFn });
        };
    }
}

module.exports = Aop;
