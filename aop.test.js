const expect = require('chai').expect;
const Aop = require('./aop');

describe('Aop', () => {
    let aop;

    let targetObj;
    let executionPoints;

    beforeEach(() => {
        aop = new Aop();

        targetObj = {
            targetFn: () => {
                executionPoints.push('targetFn');
            },
        };

        executionPoints = [];
    });

    describe('Aop.around(fnName, advice, targetObj)', () => {
        it('타깃 함수를 호출 시 어드바이스를 실행하도록 한다', () => {
            let executedAdvice = false;
            const advice = () => { executedAdvice = true; };

            aop.around('targetFn', advice, targetObj);
            targetObj.targetFn();
            /* eslint-disable no-unused-expressions */
            expect(executedAdvice).to.be.true;
        });
        it('어드바이스가 타깃 호출을 래핑', () => {
            const wrappingAdvice = (targetInfo) => {
                executionPoints.push('wrappingAdvice - 처음');
                targetInfo.fn();
                executionPoints.push('wrappingAdvice - 끝');
            };

            aop.around('targetFn', wrappingAdvice, targetObj);
            targetObj.targetFn();
            expect(executionPoints).to.deep.equal(['wrappingAdvice - 처음', 'targetFn', 'wrappingAdvice - 끝']);
        });
        it('마지막 어드바이스가 기존 어드바이스에 대해 실행되는 방식으로 체이닝 될 수 있다.', () => {
            const adviceFactory = adviceId =>
                (targetInfo) => {
                    executionPoints.push(`wrappingAdvice - 처음 ${adviceId}`);
                    targetInfo.fn();
                    executionPoints.push(`wrappingAdvice - 끝 ${adviceId}`);
                };

            aop.around('targetFn', adviceFactory('안쪽'), targetObj);
            aop.around('targetFn', adviceFactory('바깥쪽'), targetObj);
            targetObj.targetFn();

            expect(executionPoints).to.deep.equal([
                'wrappingAdvice - 처음 바깥쪽',
                'wrappingAdvice - 처음 안쪽',
                'targetFn',
                'wrappingAdvice - 끝 안쪽',
                'wrappingAdvice - 끝 바깥쪽',
            ]);
        });
    });
});
